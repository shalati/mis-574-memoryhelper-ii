﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainWindow
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainWindow))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.buttonTop = New System.Windows.Forms.ToolStripButton()
        Me.buttonPrevious = New System.Windows.Forms.ToolStripButton()
        Me.buttonNext = New System.Windows.Forms.ToolStripButton()
        Me.buttonBottom = New System.Windows.Forms.ToolStripButton()
        Me.labelTitle = New System.Windows.Forms.Label()
        Me.labelAuthor = New System.Windows.Forms.Label()
        Me.labelPublisher = New System.Windows.Forms.Label()
        Me.textTitle = New System.Windows.Forms.TextBox()
        Me.textAuthor = New System.Windows.Forms.TextBox()
        Me.textPublisher = New System.Windows.Forms.TextBox()
        Me.textStuff = New System.Windows.Forms.TextBox()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.buttonTop, Me.buttonPrevious, Me.buttonNext, Me.buttonBottom})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(787, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'buttonTop
        '
        Me.buttonTop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.buttonTop.Image = CType(resources.GetObject("buttonTop.Image"), System.Drawing.Image)
        Me.buttonTop.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.buttonTop.Name = "buttonTop"
        Me.buttonTop.Size = New System.Drawing.Size(23, 22)
        Me.buttonTop.Text = "ToolStripButton1"
        '
        'buttonPrevious
        '
        Me.buttonPrevious.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.buttonPrevious.Image = CType(resources.GetObject("buttonPrevious.Image"), System.Drawing.Image)
        Me.buttonPrevious.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.buttonPrevious.Name = "buttonPrevious"
        Me.buttonPrevious.Size = New System.Drawing.Size(23, 22)
        Me.buttonPrevious.Text = "ToolStripButton2"
        '
        'buttonNext
        '
        Me.buttonNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.buttonNext.Image = CType(resources.GetObject("buttonNext.Image"), System.Drawing.Image)
        Me.buttonNext.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.buttonNext.Name = "buttonNext"
        Me.buttonNext.Size = New System.Drawing.Size(23, 22)
        Me.buttonNext.Text = "ToolStripButton3"
        '
        'buttonBottom
        '
        Me.buttonBottom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.buttonBottom.Image = CType(resources.GetObject("buttonBottom.Image"), System.Drawing.Image)
        Me.buttonBottom.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.buttonBottom.Name = "buttonBottom"
        Me.buttonBottom.Size = New System.Drawing.Size(23, 22)
        Me.buttonBottom.Text = "ToolStripButton4"
        '
        'labelTitle
        '
        Me.labelTitle.AutoSize = True
        Me.labelTitle.Location = New System.Drawing.Point(44, 79)
        Me.labelTitle.Name = "labelTitle"
        Me.labelTitle.Size = New System.Drawing.Size(52, 26)
        Me.labelTitle.TabIndex = 1
        Me.labelTitle.Text = "Title"
        '
        'labelAuthor
        '
        Me.labelAuthor.AutoSize = True
        Me.labelAuthor.Location = New System.Drawing.Point(44, 142)
        Me.labelAuthor.Name = "labelAuthor"
        Me.labelAuthor.Size = New System.Drawing.Size(76, 26)
        Me.labelAuthor.TabIndex = 2
        Me.labelAuthor.Text = "Author"
        '
        'labelPublisher
        '
        Me.labelPublisher.AutoSize = True
        Me.labelPublisher.Location = New System.Drawing.Point(44, 214)
        Me.labelPublisher.Name = "labelPublisher"
        Me.labelPublisher.Size = New System.Drawing.Size(103, 26)
        Me.labelPublisher.TabIndex = 3
        Me.labelPublisher.Text = "Publisher"
        '
        'textTitle
        '
        Me.textTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.textTitle.Location = New System.Drawing.Point(193, 79)
        Me.textTitle.Name = "textTitle"
        Me.textTitle.Size = New System.Drawing.Size(547, 31)
        Me.textTitle.TabIndex = 4
        '
        'textAuthor
        '
        Me.textAuthor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.textAuthor.Location = New System.Drawing.Point(193, 142)
        Me.textAuthor.Name = "textAuthor"
        Me.textAuthor.Size = New System.Drawing.Size(547, 31)
        Me.textAuthor.TabIndex = 5
        '
        'textPublisher
        '
        Me.textPublisher.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.textPublisher.Location = New System.Drawing.Point(193, 214)
        Me.textPublisher.Name = "textPublisher"
        Me.textPublisher.Size = New System.Drawing.Size(547, 31)
        Me.textPublisher.TabIndex = 6
        '
        'textStuff
        '
        Me.textStuff.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.textStuff.Location = New System.Drawing.Point(49, 291)
        Me.textStuff.Multiline = True
        Me.textStuff.Name = "textStuff"
        Me.textStuff.Size = New System.Drawing.Size(691, 296)
        Me.textStuff.TabIndex = 7
        '
        'MainWindow
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(787, 637)
        Me.Controls.Add(Me.textStuff)
        Me.Controls.Add(Me.textPublisher)
        Me.Controls.Add(Me.textAuthor)
        Me.Controls.Add(Me.textTitle)
        Me.Controls.Add(Me.labelPublisher)
        Me.Controls.Add(Me.labelAuthor)
        Me.Controls.Add(Me.labelTitle)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "MainWindow"
        Me.Text = "Memory Helper II"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents buttonTop As System.Windows.Forms.ToolStripButton
    Friend WithEvents buttonPrevious As System.Windows.Forms.ToolStripButton
    Friend WithEvents buttonNext As System.Windows.Forms.ToolStripButton
    Friend WithEvents buttonBottom As System.Windows.Forms.ToolStripButton
    Friend WithEvents labelTitle As System.Windows.Forms.Label
    Friend WithEvents labelAuthor As System.Windows.Forms.Label
    Friend WithEvents labelPublisher As System.Windows.Forms.Label
    Friend WithEvents textTitle As System.Windows.Forms.TextBox
    Friend WithEvents textAuthor As System.Windows.Forms.TextBox
    Friend WithEvents textPublisher As System.Windows.Forms.TextBox
    Friend WithEvents textStuff As System.Windows.Forms.TextBox

End Class
