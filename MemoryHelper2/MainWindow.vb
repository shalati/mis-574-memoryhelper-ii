﻿Option Explicit On
Option Strict On

Imports System.IO

Public Class MainWindow

    Dim strMemConnection As String = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source = " & Application.StartupPath & "\..\..\..\Assets\Data\Memory.mdb"
    Dim strSQL As String
    Dim dtMemory As New DataTable()
    Dim intTotalRows As Integer
    Dim intCurrentRow As Integer

    ''' <summary>
    ''' Displays the current record.
    ''' </summary>
    Private Sub displayRecord()
        Me.textTitle.Text = CStr(dtMemory.Rows(intCurrentRow)("title"))
        Me.textAuthor.Text = CStr(dtMemory.Rows(intCurrentRow)("author"))
        Me.textPublisher.Text = CStr(dtMemory.Rows(intCurrentRow)("publisher"))
        Me.textStuff.Text = CStr(dtMemory.Rows(intCurrentRow)("stuff"))
    End Sub

    ' ------------------------------------------------------------------------------
    ' Event Handlers
    ' ------------------------------------------------------------------------------

    ''' <summary>
    ''' Event Handler for loading the MainWindow.
    ''' </summary>
    Private Sub MainWindow_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtMemory.Clear()
        strSQL = "SELECT * FROM Memory"
        Dim dataAdapter As New OleDb.OleDbDataAdapter(strSQL, strMemConnection)
        dataAdapter.Fill(dtMemory)
        dataAdapter.Dispose()
        intTotalRows = dtMemory.Rows.Count
        intCurrentRow = 0
        displayRecord()
    End Sub

    ''' <summary>
    ''' Event Handler for buttonTop click.
    ''' </summary>
    Private Sub buttonTop_Click(sender As Object, e As EventArgs) Handles buttonTop.Click
        intCurrentRow = 0
        displayRecord()
    End Sub

    ''' <summary>
    ''' Event Handler for buttonPrevious click.
    ''' </summary>
    Private Sub buttonPrevious_Click(sender As Object, e As EventArgs) Handles buttonPrevious.Click
        intCurrentRow = Math.Max(intCurrentRow - 1, 0)
        displayRecord()
    End Sub

    ''' <summary>
    ''' Event Handler for buttonNext click.
    ''' </summary>
    Private Sub buttonNext_Click(sender As Object, e As EventArgs) Handles buttonNext.Click
        intCurrentRow = Math.Min(intCurrentRow + 1, intTotalRows - 1)
        displayRecord()
    End Sub

    ''' <summary>
    ''' Event Handler for buttonBottom click.
    ''' </summary>
    Private Sub buttonBottom_Click(sender As Object, e As EventArgs) Handles buttonBottom.Click
        intCurrentRow = intTotalRows - 1
        displayRecord()
    End Sub
End Class
